// Instantiating new EasyHTTP class
const http = new EasyHTTP;
// User Data
const data = {
	name: 'sunny yadav',
	age: 23,
	gender: 'male',
    locality:'kesto'
}

// Update Post
http.put(
'd',
	data)

// Resolving promise for response data
.then(data => console.log(data))

// Resolving promise for error
.catch(err => console.log(err));
