const express = require('express')
const app = express()
const port = 3000
var bodyParser = require('body-parser');  
// Create application/x-www-form-urlencoded parser  
var urlencodedParser = bodyParser.urlencoded({ extended: false })  
app.use(express.static('public'));  
var mysql = require('mysql')
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'membership_gym'
})

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.get('/membership',(req ,res) => {
    var members=[];

        // connection.connect()
        connection.query('SELECT * from members', function (err, rows) {
          if (err){
            throw err
          }
          members=rows;
          // console.log('All members: ', members )
          res.send(members)
        })

        // connection.end()

           
        })

        // Retrieve a single member with memberId
app.get('/membership/:memberId', (req, res) => {
  connection.query(`SELECT * FROM members WHERE id =  ${req.params.memberId}`, function (err, result) {
    if (err) throw err;
    console.log(result);
    res.json(result);
  });
});

app.post('/membership_gym',urlencodedParser,(req,res)=>{
  console.log(req.body);
  let name=req.body.member_name
  let age =req.body.member_age
  let gender=req.body.member_gender
  let locality = req.body.member_locality
  let sql= "INSERT INTO `members`( `name`, `gender`, `age`,`locality`) VALUES ('"+name+"','"+gender+"','"+age +"','"+locality +"')"
  console.log(sql);
  connection.query(sql, function (err, result) {
    if (err){
      throw err
    }
    console.log(' member id: ', result.insertId )
    res.send('Thanks for taking interest in our GYM your Id: '+result.insertId)
  })



  // res.send('post called')
})
// Update a member with memberId
app.put('/membership/update/:memberId',(req, res) => {
  connection.connect(function(err) {
      if (err) throw err;
      var sql = `UPDATE members SET name = '${req.query.name}' WHERE id = ${req.params.memberId}`;
      connection.query(sql, function (err, result) {
        if (err){
             throw err
        }
        console.log(result.affectedRows + " record(s) updated");
      });
      res.send('message updated');
      });
      }); 
  // Delete a member with memberId
  app.delete('/membership/delete/:memberId', (req, res) => {
  connection.connect(function(err) {
      if (err) throw err;
      var sql = `DELETE FROM members WHERE Id = ${req.params.memberId}`;
      connection.query(sql, function (err, result) {
        if (err){ 
            throw err
          }
        console.log("Number of records deleted: " , result.affectedRows);
          });
       res.send('message : row deleted');
          });
    });

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
